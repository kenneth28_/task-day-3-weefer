﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Your First Name : ");
            string firstname = Console.ReadLine();
            Console.Write("Your Last Name : ");
            string lastname = Console.ReadLine();
            string fullname = firstname + " " + lastname;
            Console.WriteLine("Your Full Name is : " + fullname);
            int age = 0;
            bool gagal = true;
            while (gagal)
            {
                Console.Write("Please type your age in number :");
                gagal = (int.TryParse(Console.ReadLine(), out age)) == false;
            }
            Console.WriteLine("Your Age is : " + age);


            int yearborn = 0;
            bool failed = true;
            while (failed)
            {
                Console.Write("Type your Yearborn in number : ");
                failed = (int.TryParse(Console.ReadLine(), out yearborn)) == false;
            }
            Console.WriteLine("Your Yearborn is : " + yearborn);

            int year = +yearborn;
            int currentyear = 2021;
            while (currentyear > year)
            {
                currentyear = currentyear - 1;
                Console.WriteLine(currentyear);
            }
        }
    }
}
